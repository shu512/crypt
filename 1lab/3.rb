require '../lib/my_crypt.rb'

while(true)
  q = MyCrypt.my_rand 
  p = 2 * q + 1
  if (MyCrypt.prime?(p))
    if (MyCrypt.prime?(q))
      break
    end
  end
end

puts "p = #{p}, q = #{q}"
puts MyCrypt.dif_hel(p, q)
