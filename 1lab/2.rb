def gcd(a, b)
  if b.zero?
    a
  else
    gcd(b, a % b)
  end
end

puts 'a, b:'
a = gets.chomp.to_i
b = gets.chomp.to_i

puts gcd(a, b)
