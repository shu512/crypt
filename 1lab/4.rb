require '../lib/my_crypt.rb'

puts 'a^x mod p = y'
puts 'Введите a'
a = gets.chomp.to_i
puts 'Введите p'
p = gets.chomp.to_i
puts 'Введите y'
y = gets.chomp.to_i

x = MyCrypt.bs_gs(a, p, y)
if x.nil?
  puts 'Нет решения'
  return
end
puts "x = #{x}"
puts 'check = ' + MyCrypt.fast_pow(a, x, p).to_s
