# mental poker
require '../lib/my_crypt.rb'
require '../lib/eea.rb'

class Poker
  N_CARDS = 52
  N_CARDS_PER_TABLE = 5

  def distrib(n_cards_per_player)
    if N_CARDS < (n_cards_per_player * @n_players) + N_CARDS_PER_TABLE
      abort('Недостаточно карт')
    end
    generate_cards
    cards_crypt = []
    @cards.each_index do |i|
      cards_crypt[i] = rand(1..@p - 1)
    end
    u = cards_crypt.clone
    for k in 0...@n_players
      u.each_index do |i|
        u[i] = MyCrypt.fast_pow(u[i], @c[k], @p)
      end
      u.shuffle!
    end

    for k in 0...@n_players
      puts "Player#{k}:"
      for _i in 0...n_cards_per_player
        tmp = rand(0...u.size)
        card = u[tmp]
        for i in 0...@n_players
          card = MyCrypt.fast_pow(card, @d[i], @p)
        end
        u.delete(u[tmp])
        x = cards_crypt.index(card)
        puts "  #{@cards[x]}"
      end
    end
    puts 'table:'
    for i in 0...5
      tmp = rand(0...u.size)
      card = u[tmp]
      for i in 0...@n_players
        card = MyCrypt.fast_pow(card, @d[i], @p)
      end
      u.delete(u[tmp])
      x = cards_crypt.index(card)
      puts "  #{@cards[x]} "
    end
  end

  def generate_cards
    suits = %w[черви буби пики крести].freeze
    deck = %w[двойка тройка четвёрка пятёрка шестёрка семёрка восьмёрка девятка
              десятка валет дама король туз].freeze
    @cards = []
    suits.each do |i|
      deck.each do |j|
        @cards.push(i + ' ' + j)
      end
    end
  end

  def calc_c(p)
    loop do
      c = MyCrypt.prime
      return c if Eea.new(c, p - 1).result == 1
    end
  end

  def initialize(n_players)
    @n_players = n_players
    @c = []
    @d = []
    @p = MyCrypt.prime
    for i in 0...n_players
      @c[i] = calc_c(@p)
      @d[i] = Eea.new(@c[i], @p - 1).x
    end
  end
end
