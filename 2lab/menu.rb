require './1.rb'
require './2.rb'
require './3.rb'
require './4.rb'

if ARGV.empty?
  puts 'Usage: ruby menu.rb <filename>'
  return
end

puts 'Выберите номер шифра из предложенных:'
puts "1 Шифр Шамира\n2 Шифр Эль-Гамаля\n3 Шифр Вернама\n4 Шифр RSA"
cipher = STDIN.gets.chomp.to_i
if cipher != 1
  puts 'Выберите действие:'
  puts "1 Защифровать файл\n2 Дешифровать файл"
  value = STDIN.gets.to_i
end

case cipher
when 1
  @shamir = Shamir.new.crypt_decrypt(ARGV[0])

when 2
  case value
  when 1
    Elgamal.crypt(ARGV[0])
  when 2
    Elgamal.encrypt(ARGV[0])
  end

when 3
  case value
  when 1
    Vernam.crypt(ARGV[0])
  when 2
    Vernam.encrypt(ARGV[0])
  end

when 4
  case value
  when 1
    RSA.crypt(ARGV[0])
  when 2
    RSA.encrypt(ARGV[0])
  end
end
