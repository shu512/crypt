# rsa
require '../lib/my_crypt.rb'
require '../lib/eea.rb'
require '../lib/supp.rb'

class RSA
  def self.crypt(filename)
    generate
    output_filename = filename + '.crypt_rsa'
    output_filename_key = output_filename + '_key'
    Supp.write_file(@c, output_filename_key, ' ')
    Supp.write_file(@n, output_filename_key, "\n")
    input_file = open filename
    input_file.each do |input_line|
      input_line.each_byte do |num|
        e = MyCrypt.fast_pow(num, @d, @n)
        Supp.write_file(e, output_filename, ' ')
      end
    end
    input_file.close
  end

  def self.encrypt(filename)
    output_filename = filename + '.encrypt_rsa'
    input_file_key = open(filename + '_key')
    input_file_key.each do |e|
      str = e.split(' ')
      @c = str[0].to_i
      @n = str[1].to_i
      break
    end
    input_file_key.close
    input_file = open filename
    while true
      e = Supp.next_word(input_file)
      break if e.is_a?(File)

      m1 = MyCrypt.fast_pow(e.to_i, @c, @n)
      Supp.write_file(m1.chr, output_filename)
    end
    input_file.close
  end

  def self.generate
    @p = MyCrypt.prime
    @q = MyCrypt.prime
    @n = @p * @q
    @fi = (@p - 1) * (@q - 1)
    while true
      @d = MyCrypt.my_rand
      if Eea.new(@d, @fi).result == 1
        @c = Eea.new(@d, @fi).x
        break if @c.positive?
      end
    end
  end
end
