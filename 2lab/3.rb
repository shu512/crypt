# vernam
require '../lib/my_crypt.rb'
require '../lib/supp.rb'

class Vernam
  def self.crypt(filename)
    output_filename = filename + '.crypt_vernam'
    output_filename_key = output_filename + '_key'
    k = MyCrypt.my_rand.to_s(2)
    Supp.write_file(k, output_filename_key, "\n")
    input_file = open filename
    e = ''
    input_file.each_byte do |m|
      m = m.to_s(2)
      for i in 0..m.size - 1
        e += (m[i].to_i ^ k[i].to_i).to_s
      end
      Supp.write_file(e, output_filename, ' ')
      e = ''
    end
    input_file.close
  end

  def self.encrypt(filename)
    output_filename = filename + '.encrypt_vernam'
    m = ''
    k = ''
    crypt_file_key = open(filename + '_key')
    crypt_file_key.each do |e|
      k = e
      break
    end
    crypt_file_key.close
    crypt_file = open filename
    while true
      e = Supp.next_word(crypt_file)
      break if e.is_a?(File)

      for i in 0..e.size - 1
        m += (e[i].to_i ^ k[i].to_i).to_s
      end
      m = m.to_i(2).chr
      Supp.write_file(m, output_filename)
      m = ''
    end
    crypt_file.close
  end
end
