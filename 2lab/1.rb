require '../lib/my_crypt.rb'
require '../lib/eea.rb'
require '../lib/supp.rb'

class Shamir
  def initialize
    while true
      @p = MyCrypt.prime
      @ca = calc_c(@p)
      @da = Eea.new(@ca, @p - 1).x
      @cb = calc_c(@p)
      @db = Eea.new(@cb, @p - 1).x
      break if @da.positive? && @db.positive?
    end
  end

  def crypt_decrypt(filename)
    @filenames = []
    for i in 1..3
      @filenames.push(ARGV[0] + '.crypt' + i.to_s + '_shamir')
    end
    @filenames.push(ARGV[0] + '.encrypt_shamir')
    for i in 0..3
      File.delete(@filenames[i]) if File.exist?(@filenames[i])
    end
    input_file = open filename
    input_file.each_byte do |c|
      x1 = crypt1(c)
      Supp.write_file(x1, @filenames[0], ' ')
      x2 = crypt2(x1)
      Supp.write_file(x2, @filenames[1], ' ')
      x3 = crypt3(x2)
      Supp.write_file(x3, @filenames[2], ' ')
      x4 = crypt4(x3)
      Supp.write_file(x4.chr, @filenames[3])
    end
  end

  def calc_c(p)
    while true
      c = MyCrypt.prime
      return c if Eea.new(c, p - 1).result == 1
    end
  end

  def crypt1(m)
    MyCrypt.fast_pow(m, @ca, @p)
  end

  def crypt2(x1)
    MyCrypt.fast_pow(x1, @cb, @p)
  end

  def crypt3(x2)
    MyCrypt.fast_pow(x2, @da, @p)
  end

  def crypt4(x3)
    MyCrypt.fast_pow(x3, @db, @p)
  end
end
