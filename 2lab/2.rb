require '../lib/my_crypt.rb'
require '../lib/supp.rb'

class Elgamal
  def self.crypt(filename)
    output_filename = filename + '.crypt_elgam'
    output_filename_key = output_filename + '_key'
    generate
    Supp.write_file(@p, output_filename_key, ' ')
    Supp.write_file(@c, output_filename_key, "\n")
    input_file = open filename
    input_file.each_byte do |m|
      r = MyCrypt.fast_pow(@g, @k, @p)
      e = MyCrypt.fast_pow(@d, @k, @p) * m
      e = e % @p
      Supp.write_file(r, output_filename, ' ')
      Supp.write_file(e, output_filename, ' ')
    end
    input_file.close
  end

  def self.encrypt(filename)
    output_filename = filename + '.encrypt_elgam'
    input_file_key = open(filename + '_key')
    input_file_key.each do |e|
      str = e.split(' ')
      @p = str[0].to_i
      @c = str[1].to_i
      break
    end
    input_file_key.close
    input_file = open filename
    while true
      r = Supp.next_word(input_file)
      e = Supp.next_word(input_file)
      break if e.is_a?(File)

      m1 = MyCrypt.fast_pow(r.to_i, @p - 1 - @c, @p) * e.to_i
      m1 = m1 % @p
      Supp.write_file(m1.chr, output_filename)
    end
    input_file.close
  end

  def self.generate
    while true
      @p = MyCrypt.prime
      q = (@p - 1) / 2
      @g = MyCrypt.my_rand
      break if MyCrypt.fast_pow(@g, q, @p) != 1
    end
    @c = MyCrypt.my_rand % (@p - 1)
    @c += 1
    @d = MyCrypt.fast_pow(@g, @c, @p)
    @k = MyCrypt.my_rand % (@p - 2)
  end
end
