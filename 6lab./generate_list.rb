def read_hamilton(filename = 'hamilton')
  hamilton_list = []
  str = File.read(filename).split
  for i in 0...str.length - 1
    tmp = [].push(str[i].to_i).push(str[i + 1].to_i)
    hamilton_list.push(tmp)
  end
  hamilton_list
end

n_node = 1000
n_edge = n_node * 2
check_array = Array.new(n_node) { |i| 0 }
hamil = []
for i in 0...n_node
  while true
    new_node = rand(0...n_node)
    if check_array[new_node].zero?
      check_array[new_node] += 1
      break
    end
  end
  hamil.push(new_node)
end
tmp = hamil.inspect
tmp = tmp.slice(1...tmp.size - 1).split(', ')
hamil_str = ''
tmp.each { |i| hamil_str += i + ' ' }
File.open('hamilton', 'w') { |f| f.write(hamil_str) }

hamil = read_hamilton
my_list = []
past_node = 0
for i in 0...n_edge-n_node
  while true
    new_node = rand(0...n_node)
    next if new_node == past_node
    tmp = [].push(past_node, new_node)
    tmp2 = [].push(new_node, past_node)
    unless (hamil.include?(tmp) || my_list.include?(tmp))
      unless (hamil.include?(tmp2) || my_list.include?(tmp2))
        my_list.push(tmp)
        past_node = new_node
        break
      end
    end
  end
end

tmp = n_node * 2 - 1
tmp_str = n_node.to_s + ' ' + tmp.to_s
File.open('list', 'w') { |f| f.puts(tmp_str) }
hamil.each do |i|
  str = i[0].to_s + ' ' + i[1].to_s
  File.open('my_list', 'a') { |f| f.puts(str) }
end
my_list.each do |i|
  str = i[0].to_s + ' ' + i[1].to_s
  File.open('list', 'a') { |f| f.puts(str) }
end
