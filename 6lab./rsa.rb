def rsa_crypt(msg)
  MyCrypt.fast_pow(msg, @d, @n)
end

def rsa_encrypt(msg)
  MyCrypt.fast_pow(msg, @c, @n)
end

def rsa_generate
  p = MyCrypt.prime
  q = MyCrypt.prime
  @n = p * q
  fi = (p - 1) * (q - 1)
  while true
    @d = MyCrypt.my_rand
    if Eea.new(@d, fi).result == 1
      @c = Eea.new(@d, fi).x
      break if @c.positive?
    end
  end
end
