require './rsa.rb'
require '../lib/my_crypt.rb'

# чтение списка рёбер из файла
def read_list(filename = 'list')
  list = []
  file = File.open(filename)
  file.each_line do |i|
    @n_node = i.split[0].to_i
    @n_edge = i.split[1].to_i
    break
  end
  file.each_line do |i|
    tmp = i.split.map(&:to_i)
    list.push(tmp)
  end
  list
end

# создаём список рёбер гамильтоного цикла
def read_hamilton(filename = 'hamilton')
  hamilton_list = []
  str = File.read(filename).split
  for i in 0...str.length - 1
    tmp = [].push(str[i].to_i).push(str[i + 1].to_i)
    hamilton_list.push(tmp)
  end
  hamilton_list
end

# постройка изоморфного графа(списка рёбер) @h_list
# генерируем массив replace_node, состоящий из n_node последовательных чисел,
# а затем перемешиваем его. таким образом, получаем соответствие вершин:
# вершина i теперь имеет значение replace_node[i]
def build_izomorph(input_list, hamilton_list)
  @replace_node = Array.new(@n_node) { |i| i }
  @replace_node.shuffle!
  output_list = input_list.map do |j|
    j.map do |i|
      @replace_node[i]
    end
  end
  # также изменяем вершины гамильтонова цикл
  hamilton_list.map! do |j|
    j.map! do |i|
      @replace_node[i]
    end
  end
  output_list
end

# обратная функция для build_izomorph
def unbuild_izomorph(input_list, replace_node)
  output_list = input_list.map do |j|
    j.map do |i|
      replace_node.index(i)
    end
  end
  output_list
end

# постройка закодированного списка рёбер и гамильтонова цикла
def coding_list(input_list, hamilton_list)
  output_list = input_list.map do |input_array|
    flag = hamilton_list.include?(input_array) ? true : false
    output_array = input_array.map do |i|
      rnd = MyCrypt.my_rand
      (rnd.to_s + i.to_s).to_i
    end
    if flag
      index = hamilton_list.index(input_array)
      hamilton_list[index] = output_array
    end
    output_array
  end
  output_list
end

# обратная функция для coding_list
def decoding_list(input_list)
  output_list = input_list.map do |j|
    j.map do |i|
      i.to_s[i.to_s.length - 1].to_i
    end
  end
  output_list
end

# шифровка списка
def crypt_list(input_list)
  input_list.map do |j|
    j.map do |i|
      rsa_crypt i
    end
  end
end

# дешифровка списка
def encrypt_list(input_list)
  output_list = input_list.map do |j|
    j.map do |i|
      rsa_encrypt i
    end
  end
  output_list
end

# шифровка гамильтоного списка
def crypt_hamilton(hamilton_list)
  hamilton_list.map! do |ham|
    ham.map! do |i|
      rsa_crypt i
    end
  end
end

# дешифровка гамильтоного списка
def encrypt_hamilton(hamilton_list)
  hamilton_list.map! do |ham|
    ham.map! do |i|
      rsa_crypt i
    end
  end
end
