require 'socket'
require './list.rb'

# конвертирует полученную строку в список рёбер
def str_in_list(string)
  list = []
  string.scan(/\[\d*, \d*\]/) do |str| # ищем строку вида '[465, 123]'
    # убираем '[', ', ', ']'
    tmp = str.slice(1...str.size - 1).split(', ').map(&:to_i)
    list.push(tmp)
  end
  list
end

# конвертирует полученную строку в массив чисел
def str_in_arr(string)
  # убираем '[', ', ', ']'
  string.slice(1...string.size - 2).split(', ').map(&:to_i)
end

# проверка первого вопроса
def check_hamilton(g_list, hamilton_list)
  # проверяем, сколько раз мы были в каждой из вершин
  check_nodes = Array.new(@n_node) { |_i| 0 }
  hamilton_list.each_index do |i|
    next if i.zero?

    node = hamilton_list[i][1]
    check_nodes[node] += 1
  end
  check_nodes[hamilton_list.first[0]] += 1
  check_nodes[hamilton_list.first[1]] += 1

  check_nodes.each { |i| return false if i.zero? || i > 1 }
  true
end

# проверка второго вопроса
def check_izomorph(g_list, f_list, replace_node)
  h1_list = encrypt_list(f_list)
  h_list = decoding_list(h1_list)
  g_list_check = unbuild_izomorph(h_list, replace_node)

  return false unless g_list_check == g_list

  true
end

socket = TCPServer.new 9999 # создание сокета
alice = socket.accept # ожидаем подключения Алисы

g_list = read_list
f_list = str_in_list(alice.gets.chomp)
reply = rand(1..2)
alice.puts reply
puts "bob choice #{reply}"

case reply
when 1
  hamilton_list = str_in_list(alice.gets.chomp)
  puts 'alice send hamilton:'
  puts hamilton_list.inspect
  result = check_hamilton(g_list, hamilton_list)
  puts result
  puts 'error' unless result
when 2
  @c = alice.gets.chomp.to_i
  @n = alice.gets.chomp.to_i
  replace_node = str_in_arr(alice.gets)
  puts 'alice send:'
  puts "RSA keys: #{@c}, #{@n}"
  puts "izomorph: #{replace_node}"
  result = check_izomorph(g_list, f_list, replace_node)
  puts result
  puts 'error' unless result
end
alice.close
