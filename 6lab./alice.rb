require 'socket'
require '../lib/eea.rb'
require './list.rb'

def list_info
  izomorph_info @h_list
  #puts 'Закодированный список:'
  #puts '  ' + @h1_list.inspect + "\n\n"
  #puts 'Шифрованный список:'
  #puts '' + @f_list.inspect + "\n\n"
end

def izomorph_info(list)
  puts 'Замена вершин:'
  @replace_node.each_index do |i|
    puts '  ' + i.to_s + ' -> ' + @replace_node[i].to_s
  end
  puts 'Получен изоморфный граф:'
  puts list.inspect
end

rsa_generate

@g_list = read_list
hamilton_list_init = read_hamilton
hamilton_list = Marshal.load(Marshal.dump(hamilton_list_init))

@h_list = build_izomorph(@g_list, hamilton_list)
@h1_list = coding_list(@h_list, hamilton_list)
#puts "hamilton_coding = #{hamilton_list.inspect}"
@f_list = crypt_list(@h1_list)
crypt_hamilton(hamilton_list)
#puts "hamilton_crypt = #{hamilton_list.inspect}"
list_info

bob = TCPSocket.new 'localhost', 9999 # подключение к Бобу
bob.puts @f_list.inspect
reply = bob.gets.chomp.to_i

case reply
when 1
  bob.puts hamilton_list_init.inspect
when 2
  bob.puts @c
  bob.puts @n
  bob.puts @replace_node.inspect
end
bob.close
