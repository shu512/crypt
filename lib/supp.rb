class Supp
  def self.write_file(value, filename, add_str = '')
    File.open(filename, 'a') { |f| f.write(value.to_s + add_str) }
  end

  def self.next_word(file)
    word = ''
    file.each_char do |c|
      return word unless c != ' '
      return nil if file.eof?

      word += c
    end
  end
end
