class Eea
  attr_reader :x, :y, :result

  def initialize(a, b)
    @a = a
    @b = b
    @result = gcd
  end

  private

  # нахождение НОД
  def gcd
    eea(@a, @b, @a, 1, 0, @b, 0, 1)
  end
  # расширенный алгоритм Евклида
  def eea(init_a, init_b, a, x0, y0, b, x1, y1)
    if (a % b).zero?
      result = init_a * x1 + init_b * y1
      @x = x1
      @y = y1
      result
    else
      c = a % b
      q = a / b
      x2 = x0 - (x1 * q)
      y2 = y0 - (y1 * q)
      eea(init_a, init_b, b, x1, y1, c, x2, y2)
    end
  end
end
