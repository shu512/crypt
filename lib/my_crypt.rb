class MyCrypt
  def self.prime
    while(true)
      tmp = MyCrypt.my_rand
      return tmp if MyCrypt.prime?(tmp)
    end
  end

  def self.prime?(x) #проверка на простоту
    (2..Math.sqrt(x)).each do |i|
      return false if (x % i).zero?
    end
    return true
  end

  def self.my_rand
    rand(1..10**9)
  end

  def self.bs_gs(a, p, y) #шаг младенца, шаг великана
    arr1 = {}
    arr2 = {}
    m = Math.sqrt(p).ceil
    k = m
    (0..m - 1).each do |i|
      tmp = MyCrypt.fast_pow(a, i, p)
      tmp = (tmp * y ) % p
      arr1.store(i, tmp)
    end
    (1..k).each do |i|
      tmp = MyCrypt.fast_pow(a, i * m, p)
      arr2.store(i, tmp)
    end
  ##мб сделать объекты Pair
    arr1.sort_by { |_k, v| v }.to_h
    arr2.sort_by { |_k, v| v }.to_h
    arr1.each_value do |i|
      if arr2.rassoc(i)
        index1 = arr1.rassoc(i).first
        index2 = arr2.rassoc(i).first
        x = index2 * m - index1
        return x
      end
    end
    return nil
  end

  def self.dif_hel(p, q) #Диффи-Хеллмана
    unless MyCrypt.prime?(p)
      puts "#{p} не простое число"
      return
    end
    unless MyCrypt.prime?(q)
      puts "#{q} не простое число"
      return
    end
    while(true)
      g = rand(2 .. p - 2)
      if(fast_pow(g, q, p) != 1)
        break
      end
    end
    xa = rand(2 .. p - 2)
    xb = rand(2 .. p - 2)
    ya = MyCrypt.fast_pow(g, xa, p)
    yb = MyCrypt.fast_pow(g, xb, p)
    zab = MyCrypt.fast_pow(yb, xa, p)
    zba = MyCrypt.fast_pow(ya, xb, p)
    if zab != zba
      puts "Error: Zba != Zab. #{zba} != #{zab}"
      return nil
    end
    zab
  end

  def self.fast_pow(a, x, p) #быстрое возведение в степень по модулю
    x = p - 1 + x unless x.positive?
    temp = x.to_i % 2
    x = x >> 1
    sum = (temp == 1) ? a : 1
    value = a
    i = 1
    while x != 0
      temp = x.to_i % 2
      x = x >> 1
      value = (value**2) % p
      sum *= value * temp if temp != 0
      i += 1
    end
    sum % p
  end
end
