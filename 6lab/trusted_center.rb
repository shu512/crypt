require '../lib/my_crypt.rb'

class TrustedCenter
  TRUSTED_CENTER_DIR = 'trusted_center/'.freeze

  def initialize
    make_dir
    write_info
    write_n
    write_t
  end

  def make_dir
    Dir.mkdir(TRUSTED_CENTER_DIR) unless Dir.exist? TRUSTED_CENTER_DIR
  end

  def write_info
    File.open('trusted.dir', 'w') { |file| file.write TRUSTED_CENTER_DIR }
  end

  def write_n
    p = MyCrypt.prime
    q = MyCrypt.prime
    n = p * q
    File.open(TRUSTED_CENTER_DIR + 'n', 'w') { |file| file.write n }
  end

  def write_t
    t = 20
    File.open(TRUSTED_CENTER_DIR + 't', 'w') { |file| file.write t }
  end
end

TrustedCenter.new
