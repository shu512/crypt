require 'socket'
require '../lib/my_crypt.rb'
require '../lib/eea.rb'

def main
  trusted_center
  server = TCPServer.new 9999 # создание сокета
  loop do
    client = server.accept # ожидание подключения
    @client_name = client.gets.chomp
    if authorization client
      STDOUT.puts "#{@client_name} authorization success"
    else
      client.puts 'authorization failed'
      STDOUT.puts "#{@client_name} authorization failed"
    end
    client.close
  end
end

# считывание n и t
def trusted_center
  @trusted_center_dir = IO.read('trusted.dir')
  @n = IO.read(@trusted_center_dir + 'n').to_i
  @t = IO.read(@trusted_center_dir + 't').to_i
end

def authorization(client)
  v = IO.read(@trusted_center_dir + @client_name).to_i # считываем v из файла
  for _i in 0...@t
    x = client.gets.to_i

    e = rand(0..1)
    client.puts e

    y = client.gets.to_i
    if y.zero?
      return false
    else
      a = (y**2 % @n)
      b = (x * v**e) % @n
      return false if a != b
    end
  end
  true
end

main
