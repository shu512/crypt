require 'socket'
require '../lib/eea.rb'

def main
  @name = gets_name
  trusted_center
  generate

  server = TCPSocket.new 'localhost', 9999 # подключение к серверу
  server.puts @name
  authorization(server)
  server.close
rescue
  abort 'authorization failed'
end

def authorization(server)
  for _i in 0...@t
    r = rand(1..@n - 1)
    x = ((r % @n) * (r % @n)) % @n
    server.puts x

    e = server.gets.to_i

    y = r * @s**e
    server.puts y
  end
end

# считывание n и t
def trusted_center
  @trusted_center_dir = IO.read('trusted.dir')
  @n = IO.read(@trusted_center_dir + 'n').to_i
  @t = IO.read(@trusted_center_dir + 't').to_i
end

def generate
  Kernel.loop do
    @s = rand(1..@n - 1)
    break if Eea.new(@s, @n).result == 1
  end
  v = ((@s % @n) * (@s % @n)) % @n
  File.open(@trusted_center_dir + @name, 'w') { |file| file.write v } # запись v
end

def gets_name
  puts 'Enter your name'
  gets.chomp
end

main
