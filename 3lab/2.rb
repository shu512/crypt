class RSASignature
  def self.add_signature(filename)
    generate
    output_filename = filename + '.signature_rsa'
    m = IO.read(filename)
    y = (Digest::MD5.hexdigest m).slice(0..9).to_i(16)
    s = MyCrypt.fast_pow(y, @c, @n)
    Supp.write_file(s, output_filename, ' ')
    Supp.write_file(@d, output_filename, ' ')
    Supp.write_file(@n, output_filename, ' ')
  end

  def self.check_signature(filename)
    keys = IO.read(filename + '.signature_rsa').split
    @s = keys[0].to_i
    @d = keys[1].to_i
    @n = keys[2].to_i
    m = IO.read(filename)
    w = MyCrypt.fast_pow(@s, @d, @n)
    x = (Digest::MD5.hexdigest m).slice(0..9).to_i(16)
    x == w
  end

  def self.generate
    @p = MyCrypt.prime
    @q = MyCrypt.prime
    @n = @p * @q
    @fi = (@p - 1) * (@q - 1)
    while true
      @d = MyCrypt.my_rand
      if Eea.new(@d, @fi).result == 1
        @c = Eea.new(@d, @fi).x
        break if @c.positive?
      end
    end
  end
end

#RSASignature.add_signature(ARGV[0])
#puts RSASignature.check_signature(ARGV[0])
