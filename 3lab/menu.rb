require '../lib/my_crypt.rb'
require '../lib/supp.rb'
require '../lib/eea.rb'
require 'digest'
require './1.rb'
require './2.rb'
require './3.rb'

if ARGV.empty?
  puts 'Usage: ruby menu.rb <filename>'
  return
end

puts 'Выберите подпись:'
p "1 - Эль-Гамаль\n2 - RSA\n 3 - ГОСТ\n"
signature = STDIN.gets.to_i
p "1 - создать подпись\n2 - проверить подпись\n"
value = STDIN.gets.to_i

case signature
when 1
  case value
  when 1
    ElgamalSignature.add_signature(ARGV[0])
  when 2
    puts ElgamalSignature.check_signature(ARGV[0])
  end

when 2
  case value
  when 1
    RSASignature.add_signature(ARGV[0])
  when 2
    puts RSASignature.check_signature(ARGV[0])
  end

when 3
  case value
  when 1
    GOSTSignature.add_signature(ARGV[0])
  when 2
    puts GOSTSignature.check_signature(ARGV[0])
  end
end
