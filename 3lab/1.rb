class ElgamalSignature
  def self.add_signature(filename)
    generate
    output_filename = filename + '.signature_elgam'
    m = IO.read(filename)
    h = (Digest::MD5.hexdigest m).slice(0..9).to_i(16)
    r = MyCrypt.fast_pow(@g, @k, @p)
    u = (h - @x * r) % (@p - 1)
    k1 = Eea.new(@k, @p - 1).x
    s = (k1 * u) % (@p - 1)
    Supp.write_file(r, output_filename, ' ')
    Supp.write_file(s, output_filename, ' ')
    Supp.write_file(@y, output_filename, ' ')
    Supp.write_file(@g, output_filename, ' ')
    Supp.write_file(@p, output_filename, ' ')
  end

  def self.check_signature(filename)
    keys = IO.read(filename + '.signature_elgam').split
    r = keys[0].to_i
    s = keys[1].to_i
    y = keys[2].to_i
    g = keys[3].to_i
    p = keys[4].to_i
    m = IO.read(filename)
    h = (Digest::MD5.hexdigest m).slice(0..9).to_i(16)
    #a1 = (y**r)
    #a2 = (r**s)
    a1 = MyCrypt.fast_pow(y, r, p)
    a2 = MyCrypt.fast_pow(r, s, p)
    (a1 * a2) % p == MyCrypt.fast_pow(g, h, p)
  end

  def self.generate
    while true
      @p = MyCrypt.prime
      q = (@p - 1) / 2
      @g = MyCrypt.my_rand
      break if MyCrypt.fast_pow(@g, q, @p) != 1
    end
    @x = rand(2..@p - 2)
    @y = MyCrypt.fast_pow(@g, @x, @p)
    while true
      @k = rand(2..@p - 2)
      break if Eea.new(@k, @p - 1).result == 1
    end
  end
end

#ElgamalSignature.add_signature(ARGV[0])
#puts ElgamalSignature.check_signature(ARGV[0])
