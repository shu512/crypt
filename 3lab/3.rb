class GOSTSignature
  def self.add_signature(filename)
    output_filename = filename + '.signature_gost'
    m = IO.read(filename)
    while true
      generate
      @h = (Digest::MD5.hexdigest m).slice(0..6).to_i(16)
      break if @h < @q
    end
    while true
      @k = rand(1..@q - 1)
      @r = MyCrypt.fast_pow(@a, @k, @p) % @q
      @s = ((@k * @h) + (@x * @r)) % @q
      next if @r.zero?
      next if @s.zero?
      break
    end
    Supp.write_file(@r, output_filename, ' ')
    Supp.write_file(@s, output_filename, ' ')
    Supp.write_file(@p, output_filename, ' ')
    Supp.write_file(@q, output_filename, ' ')
    Supp.write_file(@a, output_filename, ' ')
    Supp.write_file(@y, output_filename, ' ')
  end

  def self.check_signature(filename)
    keys = IO.read(filename + '.signature_gost').split
    r = keys[0].to_i
    s = keys[1].to_i
    p = keys[2].to_i
    q = keys[3].to_i
    a = keys[4].to_i
    y = keys[5].to_i
    m = IO.read(filename)
    h = (Digest::MD5.hexdigest m).slice(0..6).to_i(16)
    return false if r >= q
    return false if s >= q

    h1 = Eea.new(h, p - 1).x
    u1 = (s * h1) % q
    u2 = (-r * h1) % q
    tmp1 = MyCrypt.fast_pow(a, u1, p)
    tmp2 = MyCrypt.fast_pow(y, u2, p)
    v = ((tmp1 * tmp2) % p) % q
    v == r
  end

  def self.generate
    while true
      @q = MyCrypt.prime
      next if @q.to_s(2)[0] == '0'

      for i in 1..1000
        @p = i * @q + 1
        next if @p.to_s(2)[0] == '0'
        break if MyCrypt.prime?(@p)
      end
      break if MyCrypt.prime?(@p)
    end
    while true
      @a = MyCrypt.my_rand + 1
      break if MyCrypt.fast_pow(@a, @q, @p) == 1
    end
    @x = rand(1..@q - 1)
    @y = MyCrypt.fast_pow(@a, @x, @p)
  end
end
