require '../lib/my_crypt.rb'
require '../lib/eea.rb'
require '../lib/supp.rb'
require 'digest'

def generate
  @p = MyCrypt.prime
  @q = MyCrypt.prime
  @N = @p * @q
  @fi = (@p - 1) * (@q - 1)
  Kernel.loop do
    @d = MyCrypt.my_rand
    if Eea.new(@d, @fi).result == 1
      @c = Eea.new(@d, @fi).x
      break
    end
  end
  Kernel.loop do
    @r = MyCrypt.my_rand
    break if Eea.new(@r, @N).result == 1
  end
end

Kernel.loop do
  Kernel.loop do
    puts 'enter your name'
    @name = gets.chomp
    break unless File.exist?(@name)

    puts 'already voited'
  end

  Kernel.loop do
    generate
    rnd = MyCrypt.my_rand
    v = rand(0..2)
    @n = rnd.to_s + v.to_s
    h = (Digest::SHA1.hexdigest @n).slice(0..9).to_i(16)
    h1 = MyCrypt.fast_pow(@r, @d, @N)
    tmp = h % @N
    h1 = (tmp * h1) % @N
    s1 = MyCrypt.fast_pow(h1, @c, @N)
    r1 = Eea.new(@r, @N).x
    s = (s1 * r1) % @N

    check = (Digest::SHA1.hexdigest @n).slice(0..9).to_i(16)
    check2 = MyCrypt.fast_pow(s, @d, @N)
    if check == check2
      Supp.write_file('', @name)
      str = IO.read('voited')
      str = str.split
      number = @n[@n.length - 1].to_i
      str[number] = str[number].to_i + 1
      File.open('voited', 'w') { |f| f.write((str[0]).to_s + "\n") }
      File.open('voited', 'a') { |f| f.write((str[1]).to_s + "\n") }
      File.open('voited', 'a') { |f| f.write((str[2]).to_s + "\n") }
      break
    end
  end
end
